$(function (){
    const socket = io();

    //obteniendo los elementos del dom desde la interface
    const $messageForm = $('#message-form');
    const $message = $('#message');
    const $chat = $('#chat');


    //obteniendo los elementos del dom desde el nickNameForm

    const $nickFrom = $('#nickForm');
    const $nickName = $('#nickName');
    const $nickError = $('#nickErro');
    const $users = $('#usernames');
    
    $nickFrom.submit(e => {
        e.preventDefault();
        socket.emit('new user', $nickName.val(), data =>{
            if(data){
                $('#nickWrap').hide();
                $('#contentWrap').show();
            }
            else{
                $nickError.html(`
                    <div class="alert alert-danger">
                        That username already exits
                    </div>
                `)
            }

            $nickName.val('');
        });
    });


    //eventos
    $messageForm.submit(e => {
        e.preventDefault();
        socket.emit('send message', $message.val(), data =>{
            $chat.append(`<p class="error">${data}</p>`)
        });
        $message.val('');
    });


    socket.on('new message', function (data){
        $chat.append('<b>'+ data.nick + '</b>:' + data.msg +'<br/>')
    })

    socket.on('usernames', data => {
        let html = '';
        for (let i = 0; i<data.length; i++){
            html += `<p>${data[i]}</p>`
        }
        $users.html(html);
    });

    socket.on('whisper', data =>{
        $chat.append(`<p class="whisper">${data.nick}: ${data.msg}</p>`)
    });

    socket.on('load old msgs', data => {
        for(let i=0; i < data.length; i++){
            displayMsg(data[i]);
        }
    })

    
    function displayMsg(data){
        $chat.append(`<p class="whisper">${data.nick}: ${data.msg}</p>`);
    };
    

})

